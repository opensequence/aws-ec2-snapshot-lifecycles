<#
.SYNOPSIS
Shared Functions shared amongst scripts


.DESCRIPTION
Shared Functions for These Suite of Scripts:

Part of a suite of scripts:
Set-EC2SnapshotLifeCycle - Sets TTL's and sets "MarkedForDeletion" Snapshot Status tag
Remove-SnapshotsMFD - Deletes any snapshots with the "MarkedForDeletion" tag set in SnapshotStatus
New-EC2VolumeBackup - Sets up automated Volume Backup using Cloudwatch Events, and sets root SnapshotTTL


.NOTES
AUTHOR  : Mitchell Gill
CREATED : 20/04/2018
VERSION : 1.0
          1.0 - Initial Revision
            <Date> - <Author>

.INPUTS
None. This script does not accept pipelines

.OUTPUTS
None. This script does not output parameters

#>

#-- Requires -Version <N>[.<n>]
#-- Requires -PSSnapin <PSSnapin-Name> [-Version <N>[.<n>]]
#-- Requires -Modules { <Module-Name> | <Hashtable> }
#-- Requires -ShellId <ShellId>
#-- Requires -RunAsAdministrator

#====================================================================================================
#                                             Parameters
#====================================================================================================
#region Parameters

#endregion Parameters
#====================================================================================================
#                                             Functions
#====================================================================================================
#region Functions

#region Set-SnapshotTTL
#Checks the Snapshot, returns a string of one of these actions:
# tagged_new tagged_MFD skipped_MFD skipped_CreatedToday skipped_OK failed_TTLTag failed_MFDTag
function Set-SnapshotTTL {
    Param(
        [Parameter(Mandatory = $true, Position = 0)]
        [psobject]
        $Snapshot,
        [Parameter(Mandatory = $true, Position = 1)]
        [string]
        $VolumeSnapshotTTL,
        [Parameter(Mandatory = $true, Position = 1)]
        [string]
        $StatusTagName,
        [Parameter(Mandatory = $true, Position = 1)]
        [string]
        $StatusTagMFD
    )

    #check if Snapshot has been marked for Deletion, then skip
    if ($Snapshot.Tags.Key -match $StatusTagName -and $Snapshot.Tags.Value -match $StatusTagMFD) {
        Write-Verbose "Snapshot: $($Snapshot.SnapshotId) is MarkedForDeletion, Skipping!"
        return "skipped_MFD"
    }
    #check if SnapshotTTL tag exists in Snapshot
    if ($Snapshot.Tags.Key -match $TTLTagName) {
        Write-Verbose "SnapshotTTL Tag Exists in: $($Snapshot.SnapshotId)"
    } else {
        #Snapshot not tagged with SnapshotTTL, tag with Volume tag
        try {
            #Create Tag Object
            $NewTag = New-Object Amazon.EC2.Model.Tag
            $NewTag.Key = $TTLTagName
            $NewTag.Value = $VolumeSnapshotTTL
            Write-Verbose "Snapshot $($Snapshot.SnapshotID) not tagged, tagging!"
            #Tag Snapshot with info from Volume
            New-EC2Tag -Resource $Snapshot.SnapshotId -Tag $NewTag
            Write-Verbose "Tagged! with TTL of $($VolumeSnapshotTTL)"
            return "tagged_new"

        } catch {
            Write-Output "Could not tag snapshot $($Snapshot.SnapshotID) with TTL of $($VolumeSnapshotTTL)"
            return "failed_TTlTag"
            #continue without ending the script
            continue
        }
    }
    #Compare Date stamp to SnapshotTTL and determine whether snapshot should be marked for deletion
    #Retrieve SnapshotTTL Value from Volume
    foreach ($tag in $Snapshot.Tags) {
        if ($tag.Key -match $TTLTagName) {
            $CurrentSnapshotTTL = $tag.Value
        }
    }
    #Get Snapshot Created Date and convert to yyyy-mm-dd format
    $SnapshotCreatedDate = $Snapshot.StartTime
    #Get Today's Date and convert to yyyy-mm-dd format
    $Today = $(Get-Date).ToUniversalTime()
    $NumberofDaysSinceSnapshot = New-TimeSpan -Start $SnapshotCreatedDate -End $Today
    #If the Number of Days Since Snapshot is 0 - Skip
    If ($NumberofDaysSinceSnapshot -eq 0) {
        Write-Verbose "Snapshot $($Snapshot.SnapshotID) Created Today, Skipping MFD Check"
        return "skipped_CreatedToday"
    }
    #If The Number of Days Since Snapshot is Greater than the TTL, Mark for Deletion
    if ($NumberofDaysSinceSnapshot -gt $CurrentSnapshotTTL) {
        Write-Verbose "Snapshot $($Snapshot.SnapshotID) Being Marked for Deletion! TTL: $($CurrentSnapshotTTL), Days Since Snapshot: $($NumberofDaysSinceSnapshot)"
        try {
            #Create Tag Object
            $NewMFDTag = New-Object Amazon.EC2.Model.Tag
            $NewMFDTag.Key = $StatusTagName
            $NewMFDTag.Value = $StatusTagMFD
            #Tag Snapshot
            New-EC2Tag -Resource $Snapshot.SnapshotId -Tag $NewMFDTag
            Write-Verbose "Tagged for Deletion!"
            return "tagged_MFD"
        } catch {
            Write-Output "Could not tag snapshot $($Snapshot.SnapshotID) for Deletion"
            return "failed_MFDTag"
            #continue without ending the script
            continue
        }
    } else {
        Write-Verbose "Snapshot $($Snapshot.SnapshotID) OK! TTL: $($CurrentSnapshotTTL), Days Since Snapshot: $($NumberofDaysSinceSnapshot)"
        return "skipped_OK"
    }
}

#endregion Set-SnapshotTTL

#region Test-RegionName
#If the supplied Region parameter matches any within the list return true
#Region List Current as of 16/04/2018
function Test-RegionName {
    Param(
        [Parameter(Mandatory = $true, Position = 0)]
        [string]
        $Region
    )
    $RegionList = "us-east-2", "us-east-1", "us-west-1", "us-west-2", "ap-northeast-1", "ap-northeast-2", "ap-northeast-3", "ap-south-1",
    "ap-southeast-1", "ap-southeast-2", "ca-central-1", "cn-north-1", "cn-northwest-1", "eu-central-1", "eu-west-1", "eu-west-2", "eu-west-3",
    "sa-east-1"
    If ($RegionList -eq $Region) {
        return $true
    }
    return $false
}

#endregion Test-RegionName


#region Test-RegionName
#If the supplied Region parameter matches any within the list return true
#Region List Current as of 16/04/2018
function Test-RegionName {
    Param(
        [Parameter(Mandatory = $true, Position = 0)]
        [string]
        $Region
    )
    $RegionList = "us-east-2", "us-east-1", "us-west-1", "us-west-2", "ap-northeast-1", "ap-northeast-2", "ap-northeast-3", "ap-south-1",
    "ap-southeast-1", "ap-southeast-2", "ca-central-1", "cn-north-1", "cn-northwest-1", "eu-central-1", "eu-west-1", "eu-west-2", "eu-west-3",
    "sa-east-1"
    If ($RegionList -eq $Region) {
        return $true
    }
    return $false
}

#endregion Test-RegionName


#region SlackFunctions

#region Out-SlackMessage

function Out-SlackMessage {
    Param(
        [Parameter(Mandatory = $true, Position = 0)]
        [System.Collections.Hashtable]
        $payload,
        [Parameter(Mandatory = $true, Position = 0)]
        [string]
        $SlackWebHookUri
    )
    Set-StrictMode -Version Latest
    try {
        Invoke-WebRequest -Body (ConvertTo-Json -Compress -InputObject $payload) -Method Post -Uri $SlackWebHookUri -UseBasicParsing | Out-Null
        Write-Output "Message sent to Slack!"
    } catch {
        $ErrorMessage = $_.Exception.Message
        Throw "Message Failed to send to Slack! error: $($ErrorMessage)"
    }


}

#endregion Out-SlackMesage

#endregion SlackFuntions

#endregion Functions
#====================================================================================================
#                                          Initialize Code
#====================================================================================================
#region Initialize Code

#endregion Initialize Code
#====================================================================================================
#                                             Main Code
#====================================================================================================
#region Main Code

#endregion Main Code