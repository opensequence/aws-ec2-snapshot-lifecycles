<#
.SYNOPSIS
Sets up automated Volume Backup using Cloudwatch Events, and sets root SnapshotTTL
WORK IN PROGRESS


.DESCRIPTION
Sets up automated Volume Backup using Cloudwatch Events, and sets root SnapshotTTL. Accepts VolumeId,
or can enumerate an InstanceId for any attached volumes

Part of a suite of scripts:
Set-EC2SnapshotLifeCycle - Sets TTL's and sets "MarkedForDeletion" Snapshot Status tag
Remove-SnapshotMFD - Deletes any snapshots with the "MarkedForDeletion" tag set in SnapshotStatus
New-EC2VolumeBackup - Sets up automated Volume Backup using Cloudwatch Events, and sets root SnapshotTTL - WORK IN PROGRESS



.NOTES
AUTHOR  : Mitchell Gill
CREATED : 14/04/2018
VERSION : 1.0
          1.0 - Initial Revision
            <Date> - <Author>

.INPUTS
None. This script does not accept pipelines

.OUTPUTS
None. This script does not output parameters

.PARAMETER

.EXAMPLE
    .\New-EC2VolumeBackup.ps1 -VolumeID abc12356789 -SnapshotTTL 10 -Schedule

    Description
    -----------
    Creates a backup schedule for the specified volume

.EXAMPLE
    .\New-EC2VolumeBackup.ps1 -InstanceID abc2343325435 -SnapshotTTL -Schedule

    Description
    -----------
    Sets a Volume Backup for all volumes connected to specified Instance

#>

#-- Requires -Version <N>[.<n>]
#-- Requires -PSSnapin <PSSnapin-Name> [-Version <N>[.<n>]]
#-- Requires -Modules { <Module-Name> | <Hashtable> }
#-- Requires -ShellId <ShellId>
#-- Requires -RunAsAdministrator

#====================================================================================================
#                                             Parameters
#====================================================================================================
#region Parameters
[CmdletBinding(SupportsShouldProcess = $True)]
Param(
    [Parameter(Mandatory = $false, Position = 0)]
    [string]$ProfileName = "mgill",

    [Parameter(Mandatory = $false, Position = 1)]
    #Region List Current as of 16/04/2019
    [ValidateSet("us-east-2", "us-east-1", "us-west-1", "us-west-2", "ap-northeast-1", "ap-northeast-2", "ap-northeast-3", "ap-south-1",
        "ap-southeast-1", "ap-southeast-2", "ca-central-1", "cn-north-1", "cn-northwest-1", "eu-central-1", "eu-west-1", "eu-west-2", "eu-west-3",
        "sa-east-1")]
    [string]$AWS_Region = "ap-southeast-2",

    [Parameter(Mandatory = $false, Position = 2)]
    [boolean]$AzureFunction = $false,

    [Parameter(Mandatory = $True, Position = 3, ParameterSetName = "VolumeId")]
    [string]$VolumeId,

    [Parameter(Mandatory = $True, Position = 3, ParameterSetName = "InstanceId")]
    [string]$InstanceId,

    [Parameter(Mandatory = $True, Position = 4)]
    [int]$SnapshotTTL,

    [Parameter(Mandatory = $True, Position = 5)]
    [string]$Schedule
)

#Sets the name of the tags used
$TTLTagName = "SnapshotTTL"
$StatusTagName = "SnapshotStatus"
$StatusTagMFD = "MarkedForDeletion"

$ErrorActionPreference = "Stop"

#endregion Parameters
#====================================================================================================
#                                             Functions
#====================================================================================================
#region Functions

#endregion Functions
#====================================================================================================
#                                          Initialize Code
#====================================================================================================
#region Initialize Code
#Load stored Credentials
try {
    Set-AWSCredential -ProfileName $ProfileName
} catch {
    Write-Error "Unable to Set AWS Credential Profile!"
}

try {
    Set-DefaultAWSRegion $AWS_Region
} catch {
    Write-Error "Unable to Set AWS Region!"
}



#endregion Initialize Code
#====================================================================================================
#                                             Main Code
#====================================================================================================
#region Main Code

#endregion Main Code