# Introduction
Provides a simple, easy to understand method for managing AWS EC2 Snapshots.
All written in Powershell with full support for serverless deployment in Azure Automation. One script can be setup and scheduled to manage all Volumes and Snapshots across your Infrastructure.

#### Note: This is a lifecycle management tool - creating snapshots is recommended to be managed using CloudWatch Events:
Create Snapshots using Cloudwatch events
https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/TakeScheduledSnapshot.html

### How it works

All Volumes and Snapshots are managed using custom tags:
The **SnapshotTTL** is the "Time To Live" for the Snapshot (in days).
The Volume has a "Parent" TTL tag set, and any new Snapshots are tagged with that TTL. A user can modify these tags on individual snapshots and **Set-EC2SnapshotLifeCycle** will honour these tags and not overwrite them. **Set-EC2SnapshotLifeCycle**  will also tag any Snapshots that have an expired TTL with a **SnapshotStatus** of "MarkedForDeletion".

**Remove-SnapshotsMFD** has been designed to run seperately and perform the actual snapshot delete operation. It will delete any snapshot tagged with a **SnapshotStatus** of "MarkedForDeletion".

### Scripts:
* **Set-EC2SnapshotLifeCycle** - Sets TTL's and sets "MarkedForDeletion" Snapshot Status tag
* **Remove-SnapshotsMFD** - Deletes any snapshots with the "MarkedForDeletion" tag set in SnapshotStatus. *Supports the -WhatIf parameter*
* **Shared-Functions** - dot sourced functions used across scripts

### Azure Automation Variants:
Ensure you import the AWSPowershell module into your Automation account as per this guide:
https://docs.microsoft.com/en-us/azure/automation/automation-runbook-gallery

* **Set-EC2SnapshotLifeCycle-runbook** - Customised to support loading credentials through Azure
* **Remove-SnapshotsMFD-runbook** - Customised to support loading credentials through Azure. *Supports the -WhatIf parameter*

### Work In Progress
* **New-EC2VolumeBackup**


# Getting Started

1. Set up Volume Backups using your preferred method (Cloudwatch Events are highly recommended: https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/TakeScheduledSnapshot.html)
2. Tag any Volumes that you want managed with the **SnapshotTTL** tag and set your default retention time (in days) as the tags value
3. Execute the scripts according to your desired schedule to manage

# Syntax

**Set-EC2SnapshotLifeCycle**

```
SYNTAX
    Set-EC2SnapshotLifeCycle.ps1 [-AWSRegion] <String> [-ProfileName] <String> [[-SlackWebHookUri] <String>] [-WhatIf] [-Confirm] [<CommonParameters>]

    Set-EC2SnapshotLifeCycle.ps1 [-AWSRegion] <String> [-AccessKey] <String> [-AccessSecret] <String> [[-SlackWebHookUri] <String>] [-WhatIf] [-Confirm] [<CommonParameters>]
```

**Remove-SnapshotsMFD**
```
SYNTAX
    Remove-SnapshotsMFD.ps1 [-AWSRegion] <String> [-ProfileName] <String> [[-SlackWebHookUri] <String>] [-WhatIf] [-Confirm] [<CommonParameters>]

    Remove-SnapshotsMFD.ps1 [-AWSRegion] <String> [-AccessKey] <String> [-AccessSecret] <String> [[-SlackWebHookUri] <String>] [-WhatIf] [-Confirm] [<CommonParameters>]
```

**Set-EC2SnapshotLifeCycle-runbook**
```
SYNTAX
    Set-EC2SnapshotLifeCycle-runbook.ps1 [-AWSRegion] <String> [-AzureAutomationCredential] <PSCredential> [[-SlackWebHookUri] <PSCredential>] [-WhatIf] [-Confirm] [<CommonParameters>]
```

**Remove-SnapshotsMFD-runbook**
```
SYNTAX
    Remove-SnapshotsMFD-runbook.ps1 [-AWSRegion] <String> [-AzureAutomationCredential] <PSCredential> [[-SlackWebHookUri] <PSCredential>] [-WhatIf] [-Confirm] [<CommonParameters>]
```


# IAM permissions

**Set-EC2SnapshotLifeCycle**
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "ec2:DescribeVolumeStatus",
                "ec2:DescribeTags",
                "ec2:CreateTags",
                "ec2:DescribeVolumes",
                "ec2:DescribeSnapshots"
            ],
            "Resource": "*"
        }
    ]
}
```

**Remove-SnapshotsMFD**
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "ec2:DeleteSnapshot",
                "ec2:DescribeVolumeStatus",
                "ec2:DescribeTags",
                "ec2:CreateTags",
                "ec2:DescribeVolumes",
                "ec2:DescribeSnapshots"
            ],
            "Resource": "*"
        }
    ]
}
```

## TODO's
* Add support for Microsoft Teams
* Improved slack functions
* Improved Verbose output
* Improved Messaging outputs
