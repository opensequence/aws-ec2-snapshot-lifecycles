<#
.SYNOPSIS
Azure Runbook Edition - Permenantly DELETES all Snapshots tagged with a "SnapshotStatus" of "MarkedForDeletion" within the specified AWS account (and region).
Supports -WhatIf


.DESCRIPTION
Azure Runbook Edition - Permenantly DELETES all Snapshots tagged with a "SnapshotStatus" of "MarkedForDeletion" within the specified AWS account (and region).
Supports -WhatIf

Part of a suite of scripts:
Set-EC2SnapshotLifeCycle - Sets TTL's and sets "MarkedForDeletion" Snapshot Status tag
Remove-SnapshotsMFD - Deletes any snapshots with the "MarkedForDeletion" tag set in SnapshotStatus
New-EC2VolumeBackup -  WORK IN PROGRESS - Sets up automated Volume Backup using Cloudwatch Events, and sets root SnapshotTTL

Requirements:
Ensure you import the AWSPowershell module into your Automation account as per this guide:
https://docs.microsoft.com/en-us/azure/automation/automation-runbook-gallery

Create Snapshots using external method (like Cloudwatch events)
https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/TakeScheduledSnapshot.html


.NOTES
AUTHOR  : Mitchell Gill
CREATED : 14/04/2018
VERSION : 1.0
          1.0 - Initial Revision
            <Date> - <Author>

.INPUTS
None. This script does not accept pipelines

.OUTPUTS
None. This script does not output parameters

.PARAMETER AWSRegion
The name of the AWS Region to be targetted. Can be a comma seperated list.

.PARAMETER AzureAutomationCredential
The name of the Saved Azure Automation Credential Object

.PARAMETER SlackWebHookUri
The name of the Saved Azure Automation Credential Object
Posts final results to default Slack channel

.EXAMPLE
    .\Remove-SnapshotsMFD.ps1

    Description
    -----------
    This is the default execution

.EXAMPLE
    .\Remove-SnapshotMFD.ps1 -WhatIf

    Description
    -----------
    Displays the Results of the execution without taking any action

#>

#-- Requires -Version <N>[.<n>]
#-- Requires -PSSnapin <PSSnapin-Name> [-Version <N>[.<n>]]
#-- Requires -Modules { <Module-Name> | <Hashtable> }
#-- Requires -ShellId <ShellId>
#-- Requires -RunAsAdministrator

#====================================================================================================
#                                             Parameters
#====================================================================================================
#region Parameters
[CmdletBinding(SupportsShouldProcess = $True)]
Param(

    [Parameter(Mandatory = $True)]
    [string]$AWSRegion,

    [Parameter(Mandatory = $True)]
    [pscredential]$AzureAutomationCredential,

    [Parameter(Mandatory = $false)]
    [pscredential]$SlackWebHookUri

)

#Sets the name of the tags used
$StatusTagName = "SnapshotStatus"
$StatusTagMFD = "MarkedForDeletion"

$ErrorActionPreference = "Stop"

#endregion Parameters

#====================================================================================================
#                                             Dot Source
#====================================================================================================
#region dotsource

#Import Shared functions
. .\Shared-Functions.ps1

#endregion dotsource

#====================================================================================================
#                                             Functions
#====================================================================================================
#region Functions

#endregion Functions
#====================================================================================================
#                                          Initialize Code
#====================================================================================================
#region Initialize Code

#Set the AWS Profile
try {
    Set-AWSCredential -AccessKey $($AzureAutomationCredential.Username) -SecretKey $($AzureAutomationCredential.GetNetworkCredential().Password)
} catch {
    $ErrorMessage = $_.Exception.Message
    Write-Error "Unable to Set AWS Credential Error: $($ErrorMessage)"
}


#Define Class for Snapshot Reporting
Class EC2Snapshot {
    [string]$SnapshotID
    [string]$SnapshotState
    [string]$ParentVolumeID
}

#endregion Initialize Code
#====================================================================================================
#                                             Main Code
#====================================================================================================
#region Main Code
#Split regions into array
$RegionsArray = $AWSRegion -split ","
#If multiple regions are specified do each one
foreach ($Region in $RegionsArray) {

    #Set the Default Region for Session
    try {
        if (-not(Test-RegionName -Region $Region)) {
            Throw "$($Region) is not a Valid Region"
        }
        Write-Verbose "Setting Region to $($Region)..."
        Set-DefaultAWSRegion $Region
    } catch {
        Write-Output "Unable to Set AWS Region $($Region)! Error: $($_.Exception.Message)"
        #If error, skip this region and go to the next one
        continue
    }
    $startTime = Get-Date
    Write-Output "Started: $($startTime)"
    Write-Output ""

    #Create List of Custom Class (EC2Snapshot)
    $ReportSnapshots = New-Object "System.Collections.Generic.List[EC2Snapshot]"
    #Define Counters for Final Summary
    $success_deleted_Total = 0
    $failed_delete_Total = 0

    try {
        #Retrieve all snapshots Marked For Deletion
        $Snapshots = Get-EC2Snapshot -Filter @{Name = "tag:$($StatusTagName)"; Value = "$($StatusTagMFD)"}
    } catch {
        Write-Error "Unable to Retrieve Snapshot list!"
    }
    #For each Snapshot, Delete Snapshot
    foreach ($Snapshot in $Snapshots) {
        #Delete the Snapshot (If WhatIf set, only report)
        If ($PSCmdlet.ShouldProcess("$($Snapshot.SnapshotId)", "Deleting Snapshot")) {
            try {
                Remove-EC2Snapshot -SnapshotId $Snapshot.SnapshotId -Force
                Write-Verbose "Really deleting $($Snapshot.SnapshotId)"
                $Result = "success_deleted"
            } catch {
                Write-Output "Unable to Delete Snapshot: $($Snapshot.SnapshotId)"
                $Result = "failed_delete"
            }

        }
        #Create EC2Snapshot Object, assign values then add to list
        $EC2Snapshot = New-Object -TypeName "EC2Snapshot"
        #Assign Values to Reporting List
        $EC2Snapshot.SnapshotID = $Snapshot.SnapshotId
        $EC2Snapshot.ParentVolumeID = $Snapshot.VolumeId
        $EC2Snapshot.SnapshotState = $Result
        $ReportSnapshots.Add($EC2Snapshot)
        #Increment Appropriate Counters
        if ($result -match "success_deleted") {
            $success_deleted_Total++
        } elseif ($result -match "failed_delete") {
            $failed_delete_Total++
        }

    }

    $finishTime = Get-Date
    $commandRunTime = New-TimeSpan -Start $startTime -End $finishTime
    #Reporting
    Write-Output "Finished: $($finishTime)"
    Write-Output "Command Took $($commandRunTime.TotalSeconds) seconds"
    Write-Output ""
    Write-Output "Results: "
    Write-Output "Total Snapshots Processed: $($($ReportSnapshots).Count)"
    Write-Output "Snapshots Successfully deleted: $($success_deleted_Total)"
    Write-Output "Snapshots that failed deletion: $($failed_delete_Total)"
    Write-Output ""
    Write-Output "Details:"
    $ReportSnapshots

    #Post Results to Slack Channel (If Parameter set)
    if ($SlackWebHookUri) {

        $payload = @{
            "username" = "Remove-SnapshotsMFD"
            "text"     = "Finished (UTC): $($finishTime)
    *Results:*
    *Region:* $($Region)
    *Total Snapshots Processed:* $($($ReportSnapshots).Count)
    *Snapshots Successfully deleted:* $($success_deleted_Total)
    *Snapshots that failed deletion:* $($failed_delete_Total)"
            "mrkdwn"   = "true"

        }
        try {
            Out-SlackMessage -payload $payload -SlackWebHookUri $($SlackWebHookUri.GetNetworkCredential().Password)
        } catch {
            $ErrorMessage = $_.Exception.Message
            Write-Output "Message Failed to send to Slack! error: $($ErrorMessage)"
        }
    }
}
#endregion Main Code

#====================================================================================================
#                                             Post Script Cleanup
#====================================================================================================
#region cleanup
try {
    Write-Verbose "Cleaning up..."
    #Cleanup-AWSProfile -Profile $Profile
} catch {
    Write-Error "Problem Occurred during Cleanup Phase!"
}

#endregion cleanup
