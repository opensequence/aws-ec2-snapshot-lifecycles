<#
.SYNOPSIS
Retreives all Volumes tagged with a "SnapshotTTL" within the specified AWS account (and region).
tags new snapshots with the root Volume TTL and marks expired Snapshots as "Marked For Deletion"


.DESCRIPTION
Retreives all Volumes tagged with a "SnapshotTTL" within the specified AWS account (and region).
tags new snapshots with the root Volume TTL and marks expired Snapshots as "Marked For Deletion"

Part of a suite of scripts:
Set-EC2SnapshotLifeCycle - Sets TTL's and sets "MarkedForDeletion" Snapshot Status tag
Remove-SnapshotsMFD - Deletes any snapshots with the "MarkedForDeletion" tag set in SnapshotStatus
New-EC2VolumeBackup -  WORK IN PROGRESS - Sets up automated Volume Backup using Cloudwatch Events, and sets root SnapshotTTL


Create Snapshots using external method (like Cloudwatch events)
https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/TakeScheduledSnapshot.html



.NOTES
AUTHOR  : Mitchell Gill
CREATED : 06/04/2018
VERSION : 1.0
          1.0 - Initial Revision
            <Date> - <Author>

.INPUTS
None. This script does not accept pipelines

.OUTPUTS
None. This script does not output parameters

.PARAMETER AWSRegion
The name of the AWS Region to be targetted. Can be a comma seperated list.

.PARAMETER ProfileName
The name of a saved AWS Profile

.PARAMETER AccessKey
AWS Access Key ID (requires Access Secret)

.PARAMETER AccessSecret
AWS Secret

.PARAMETER SlackWebHookUri
Posts final results to default Slack channel


.EXAMPLE
    .\Set-EC2SnapshotLifeCycles.ps1 -AWSRegion "ap-southeast-2" -ProfileName "profile"

    Description
    -----------
    Executes script within the specified region, using a saved AWS Profile.

.EXAMPLE
    .\Set-EC2SnapshotLifeCycles.ps1 -AWSRegion "ap-southeast-2" -AccessKey "AKIFSSJJHW393" -AccessSecret "23423543tgdfgfd"

    Description
    -----------

    .EXAMPLE
    .\Set-EC2SnapshotLifeCycles.ps1 -AWSRegion "ap-southeast-2" -ProfileName "profile" -SlackWebHookUri "https://webhook.slack.com/983heahkjsdfsdf"

    Description
    -----------
    Executes script within the specified region, using a saved AWS Profile, with results set to Slack via the provided Webhook.

    .EXAMPLE
    .\Set-EC2SnapshotLifeCycles.ps1 -AWSRegion "ap-southeast-2,us-east-1" -ProfileName "profile" -SlackWebHookUri "https://webhook.slack.com/983heahkjsdfsdf"

    Description
    -----------
    Executes script within the specified region, using a saved AWS Profile, with results set to Slack via the provided Webhook.
#>

#-- Requires -Version <N>[.<n>]
#-- Requires -PSSnapin <PSSnapin-Name> [-Version <N>[.<n>]]
#-- Requires -Modules { <Module-Name> | <Hashtable> }
#-- Requires -ShellId <ShellId>
#-- Requires -RunAsAdministrator

#TODO One Collective Output for all Regions
#TODO Nicer Slack message output (and one message for all regions)
#TODO Send message to Microsoft Teams


#====================================================================================================
#                                             Parameters
#====================================================================================================
#region Parameters
[CmdletBinding(SupportsShouldProcess = $True)]
Param(

    [Parameter(Mandatory = $True, Position = 0)]
    [string]$AWSRegion,

    [Parameter(Mandatory = $True, ParameterSetName = "ProfileName", Position = 1)]
    [string]$ProfileName,

    [Parameter(Mandatory = $True, ParameterSetName = "AWSCredential", Position = 1)]
    [string]$AccessKey,

    [Parameter(Mandatory = $True, ParameterSetName = "AWSCredential", Position = 2)]
    [string]$AccessSecret,

    [Parameter(Mandatory = $false, Position = 3)]
    [string]$SlackWebHookUri

)

#Sets the name of the tags used
$TTLTagName = "SnapshotTTL"
$StatusTagName = "SnapshotStatus"
$StatusTagMFD = "MarkedForDeletion"

#Sets Error Action
$ErrorActionPreference = "Stop"

#endregion Parameters

#====================================================================================================
#                                             Dot Source
#====================================================================================================
#region dotsource

#Import Shared functions
. .\Shared-Functions.ps1

#endregion dotsource

#====================================================================================================
#                                             Functions
#====================================================================================================
#region Functions


#endregion Functions
#====================================================================================================
#                                          Initialize Code
#====================================================================================================
#region Initialize Code

#Decide how to retreive AWS Credentials based on parameters
if ($ProfileName) {
    #Load stored Credentials
    try {
        Set-AWSCredential -ProfileName $ProfileName
    } catch {
        $ErrorMessage = $_.Exception.Message
        Write-Error "Unable to Set AWS Credential Profile! Error: $($ErrorMessage)"
    }
} elseif ($AccessKey) {
    #Set Provided Keys
    try {
        Write-Verbose "Setting Profile using provided Keys: $($Profile)"
        Set-AWSCredential -AccessKey "$($AccessKey)" -SecretKey "$($AccessSecret)"
    } catch {
        $ErrorMessage = $_.Exception.Message
        Write-Error "Unable to Set AWS Credential Profile! Error: $($ErrorMessage)"
    }
}


#Define Class for Snapshot Reporting
Class EC2Snapshot {
    [string]$SnapshotID
    [string]$SnapshotState
    [string]$ParentVolumeID
}

#endregion Initialize Code
#====================================================================================================
#                                             Main Code
#====================================================================================================
#region Main Code
#Split regions into array
$RegionsArray = $AWSRegion -split ","
#If multiple regions are specified do each one
foreach ($Region in $RegionsArray) {

    #Set the Default Region for Session
    try {
        if (-not(Test-RegionName -Region $Region)) {
            Throw "$($Region) is not a Valid Region"
        }
        Write-Verbose "Setting Region to $($Region)..."
        Set-DefaultAWSRegion $Region
    } catch {
        Write-Output "Unable to Set AWS Region $($Region)! Error: $($_.Exception.Message)"
        #If error, skip this region and go to the next one
        continue
    }

    #Get all Volumes tagged with a SnapshotTTL
    $startTime = $(Get-Date).ToUniversalTime()
    Write-Output "Started (UTC): $($startTime)"
    Write-Output ""
    #Synthentic Progress Bar while waiting for the Volumes to load from AWS
    Write-Progress -Activity "Retrieving Tagged Volumes..." -Status "Please wait..."

    try {
        Write-Output "Retrieving tagged EC2 Volumes from $($Region)..."
        $Volumes = Get-EC2Volume -Filter @{Name = "tag-key"; Value = $TTLTagName}
        Write-Verbose "Retrieved $($Volumes.Count) Volumes:"
        Write-Verbose "$($Volumes.VolumeId)"
    } catch {
        $ErrorMessage = $_.Exception.Message
        Write-Error "Unable to Retrieve Volume list! Error: $($ErrorMessage)"
    }
    #Completing Progress Bar
    Write-Progress -Activity "Retrieving Tagged Volumes..." -Completed -Status "All done."

    #Total Volume Count for Report:
    $ReportVolumeCount = $Volumes.Count
    #Create List of Custom Class (EC2Snapshot)
    $ReportSnapshots = New-Object "System.Collections.Generic.List[EC2Snapshot]"
    #Define Counters for Final Summary
    $Skipped_Total = 0
    $Tagged_new_Total = 0
    $Tagged_MFD_Total = 0
    $failed_TTLTag_Total = 0
    $failed_MFDTag_Total = 0

    #for each Volume found, Retrieve all Snapshots for Volume
    foreach ($Volume in $Volumes) {
        Write-Verbose "Retrieving Snapshots for Volume: $($Volume.VolumeId)"
        #Retrieve SnapshotTTL Value from Volume
        foreach ($tag in $Volume.Tags) {
            if ($tag.Key -match $TTLTagName) {
                $VolumeSnapshotTTL = $tag.Value
            }
        }
        try {
            #Retrieve all COMPLETED snapshots for Volume
            $Snapshots = Get-EC2Snapshot -Filter @{Name = "volume-id"; Value = "$($Volume.VolumeId)"}, @{Name = "status"; Value = "completed"}

        } catch {
            $ErrorMessage = $_.Exception.Message
            Write-Output "Unable to Retrieve Snapshot list! for Volume: $($Volume.VolumeId)"
            Write-Output "Error: $($ErrorMessage)"
        }
        #For each Snapshot, Check if Tagged and if not Tag, and then check if cleanup required.
        #set counter for progress bar
        $snapshotCount = 0
        foreach ($Snapshot in $Snapshots) {
            #Snapshot Progress Bar
            $snapshotCount++
            $percentProgress = $(($snapshotCount / $Snapshots.Count) * 100)
            Write-Progress -Activity "Processing Snapshots" -Status "$($percentProgress)% Complete:" -PercentComplete $percentProgress;
            $Result = Set-SnapshotTTL -Snapshot $Snapshot -VolumeSnapshotTTL $VolumeSnapshotTTL -StatusTagName $StatusTagName -StatusTagMFD $StatusTagMFD
            #Create EC2Snapshot Object, assign values then add to list
            $EC2Snapshot = New-Object -TypeName "EC2Snapshot"
            #Assign Values to Reporting List
            $EC2Snapshot.SnapshotID = $Snapshot.SnapshotId
            $EC2Snapshot.ParentVolumeID = $Volume.VolumeId
            $EC2Snapshot.SnapshotState = $Result
            $ReportSnapshots.Add($EC2Snapshot)
            #Increment Appropriate Counters
            if (($result -match "skipped_OK") -or ($result -match "skipped_MFD") -or ($result -match "skipped_CreatedToday")) {
                $Skipped_Total++
            } elseif ($result -match "Tagged_New") {
                $Tagged_New_Total++
            } elseif ($result -match "Tagged_MFD") {
                $Tagged_MFD_Total++
            } elseif ($result -match "failed_TTLTag") {
                $failed_TTLTag_Total++
            } elseif ($result -match "failed_MFDTag") {
                $failed_MFDTag_Total++
            }

        }

    }
    $finishTime = $(Get-Date).ToUniversalTime()
    $commandRunTime = New-TimeSpan -Start $startTime -End $finishTime
    #Reporting
    Write-Output "Finished (UTC): $($finishTime)"
    Write-Output "Command Took $($commandRunTime.TotalSeconds) seconds"
    Write-Output ""
    Write-Output "Results: "
    Write-Output "Volumes Processed: $($ReportVolumeCount)"
    Write-Output "Total Snapshots Processed: $($($ReportSnapshots).Count)"
    Write-Output "Snapshots Skipped with no Action: $($Skipped_Total)"
    Write-Output "New Snapshots Tagged: $($tagged_new_Total)"
    Write-Output "Snapshots Tagged for Deletion: $($tagged_MFD_Total)"
    Write-Output ""
    Write-Output "Details:"
    $ReportSnapshots

    #Post Results to Slack Channel (If Parameter set)
    if ($SlackWebHookUri) {
        $payload = @{
            "username" = "Set-EC2SnapshotLifeCycle"
            "text"     = "Finished (UTC): $($finishTime)
    *Results:*
    *Region:* $($Region)
    *Volumes Processed:* $($ReportVolumeCount)
    *Total Snapshots Processed:* $($($ReportSnapshots).Count)
    *Snapshots Skipped with no Action:* $($Skipped_Total)
    *New Snapshots Tagged:* $($tagged_new_Total)
    *Snapshots Tagged for Deletion:* $($tagged_MFD_Total)"
            "mrkdwn"   = "true"

        }
        try {
            Out-SlackMessage -payload $payload -SlackWebHookUri $SlackWebHookUri
        } catch {
            $ErrorMessage = $_.Exception.Message
            Write-Output "Message Failed to send to Slack! error: $($ErrorMessage)"
        }
    }
}#Region foreach
#endregion Main Code

#====================================================================================================
#                                             Post Script Cleanup
#====================================================================================================
#region cleanup
try {
    Write-Verbose "Cleaning up..."
    #Cleanup-AWSProfile -Profile $Profile
} catch {
    Write-Error "Problem Occurred during Cleanup Phase!"
}

#endregion cleanup